@component('mail::message')
# Hi, {{$user->name}}

Someone Has view Your Profile & for more info login your Account

@component('mail::button', ['url' => 'https://www.google.com/'])
Check Now
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
